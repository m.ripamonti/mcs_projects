# Autori

* Christian Bernasconi 816423
* Gabriele Ferrario 817518
* Riccardo Pozzi 807857
* Marco Ripamonti 806785

## Progetti Metodi del Calcolo Scientifico

In questa repository sono stati raccolti i codici sviluppati per i progetti di
Metodi del Calcolo Scientifico.

Nella cartella `proj1` sono presenti i file necessari per il Progetto 1.
Nella cartella `proj2` sono presenti i file necessari per il Progetto 2.

### Progetto 1: Sistemi lineari con matrici sparse simmetriche e definite positive
In questo progetto sono stati confrontati i risolutori di matrici sparse, simmetriche e
definite positive con il metodo di Cholesky in ambiente Matlab, Octave e nei
sistemi operativi Windows e Linux.

### Progetto 2: Implementazione DCT2 e Implementazione di un algoritmo di compressione
Il progetto è diviso in due parti entrambe realizzate con Python:
- part1: Implementazione della DCT2 come vista a lezione
- part2: Implementazione di un algoritmo di compressione simile al JPEG e realizzazione di un'interfaccia per mostrarne l'effetto.


## Istruzioni per l'esecuzione:
Progetto 1:

    Tutti i codici sono eseguibili direttamente in Matlab e Octave.

Progetto 2:

    1. Scaricare ed installare Python al seguente url:
        https://www.python.org/downloads/ (consiglio di spuntare: Add Python xx to PATH)
        
    2. Posizionarsi nella folder: 
        .\mcs_projects\proj2\python
        
    3. Creare un Virtual Environment: 
        python -m venv venv_mcs
        
    4. Attivare l'enviroment:
         .\venv_mcs\scripts\activate
        
        Nota: in windows la cartella si chiama scripts, mentre il linux bin
        
    5. Installare i requirements:
        pip install -r requirements.txt
        
        Nota: le fasi precedenti sono da eseguire solo la prima volta
    
    6. Eseguire il codice:
    
        - parte 1: python .\part1\src\main.py
        
        - parte 2: python .\part2\src\main.py
        
        
        
            